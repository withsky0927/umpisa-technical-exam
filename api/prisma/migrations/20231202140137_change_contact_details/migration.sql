/*
  Warnings:

  - You are about to alter the column `contactDetailsId` on the `ContactDetail` table. The data in that column could be lost. The data in that column will be cast from `VarChar(26)` to `SmallInt`.
  - You are about to alter the column `contactDetailsId` on the `PersonContactDetail` table. The data in that column could be lost. The data in that column will be cast from `VarChar(26)` to `SmallInt`.

*/
-- DropForeignKey
ALTER TABLE `PersonContactDetail` DROP FOREIGN KEY `PersonContactDetail_contactDetailsId_fkey`;

-- AlterTable
ALTER TABLE `ContactDetail` MODIFY `contactDetailsId` SMALLINT NOT NULL;

-- AlterTable
ALTER TABLE `PersonContactDetail` MODIFY `contactDetailsId` SMALLINT NOT NULL;

-- AddForeignKey
ALTER TABLE `PersonContactDetail` ADD CONSTRAINT `PersonContactDetail_contactDetailsId_fkey` FOREIGN KEY (`contactDetailsId`) REFERENCES `ContactDetail`(`contactDetailsId`) ON DELETE RESTRICT ON UPDATE CASCADE;
