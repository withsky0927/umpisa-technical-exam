-- DropIndex
DROP INDEX `PersonContactDetail_createdBy_updatedBy_idx` ON `PersonContactDetail`;

-- CreateIndex
CREATE INDEX `PersonContactDetail_createdBy_updatedBy_contactDetailsId_idx` ON `PersonContactDetail`(`createdBy`, `updatedBy`, `contactDetailsId`);
