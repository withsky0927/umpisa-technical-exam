-- CreateTable
CREATE TABLE `Person` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `personId` VARCHAR(26) NOT NULL,
    `firstName` VARCHAR(50) NOT NULL,
    `middleName` VARCHAR(50) NULL,
    `lastName` VARCHAR(50) NOT NULL,
    `createdBy` VARCHAR(26) NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedBy` VARCHAR(26) NULL,
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `Person_personId_key`(`personId`),
    INDEX `Person_createdBy_updatedBy_idx`(`createdBy`, `updatedBy`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `ContactDetail` (
    `contactDetailsId` VARCHAR(26) NOT NULL,
    `name` VARCHAR(50) NOT NULL,
    `description` TEXT NULL,

    UNIQUE INDEX `ContactDetail_contactDetailsId_key`(`contactDetailsId`),
    UNIQUE INDEX `ContactDetail_name_key`(`name`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `PersonContactDetail` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `personContactDetailsId` VARCHAR(26) NOT NULL,
    `personId` VARCHAR(26) NOT NULL,
    `contactDetailsId` VARCHAR(26) NOT NULL,
    `value` VARCHAR(255) NOT NULL,
    `createdBy` VARCHAR(26) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedBy` VARCHAR(26) NULL,
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `PersonContactDetail_personContactDetailsId_key`(`personContactDetailsId`),
    UNIQUE INDEX `PersonContactDetail_personId_key`(`personId`),
    UNIQUE INDEX `PersonContactDetail_value_key`(`value`),
    INDEX `PersonContactDetail_createdBy_updatedBy_idx`(`createdBy`, `updatedBy`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `User` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `userId` VARCHAR(26) NOT NULL,
    `personId` VARCHAR(26) NOT NULL,
    `username` VARCHAR(255) NOT NULL,
    `password` TEXT NOT NULL,
    `createdBy` VARCHAR(26) NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedBy` VARCHAR(26) NULL,
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `User_userId_key`(`userId`),
    UNIQUE INDEX `User_personId_key`(`personId`),
    UNIQUE INDEX `User_username_key`(`username`),
    INDEX `User_createdBy_updatedBy_idx`(`createdBy`, `updatedBy`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Token` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `tokenId` VARCHAR(26) NOT NULL,
    `userId` VARCHAR(26) NOT NULL,
    `type` ENUM('refresh', 'reset') NOT NULL,
    `value` TEXT NOT NULL,
    `createdBy` VARCHAR(26) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedBy` VARCHAR(26) NULL,
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `Token_tokenId_key`(`tokenId`),
    INDEX `Token_createdBy_updatedBy_idx`(`createdBy`, `updatedBy`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `UsersNotes` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `userId` VARCHAR(26) NOT NULL,
    `noteId` VARCHAR(26) NOT NULL,

    INDEX `UsersNotes_userId_noteId_idx`(`userId`, `noteId`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- CreateTable
CREATE TABLE `Note` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `noteId` VARCHAR(26) NOT NULL,
    `title` VARCHAR(100) NOT NULL,
    `content` LONGTEXT NOT NULL,
    `date` DATE NOT NULL,
    `isImportant` TINYINT NOT NULL DEFAULT 0,
    `createdBy` VARCHAR(26) NOT NULL,
    `createdAt` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updatedBy` VARCHAR(26) NULL,
    `updatedAt` DATETIME(3) NOT NULL,

    UNIQUE INDEX `Note_noteId_key`(`noteId`),
    INDEX `Note_createdBy_updatedBy_idx`(`createdBy`, `updatedBy`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `PersonContactDetail` ADD CONSTRAINT `PersonContactDetail_personId_fkey` FOREIGN KEY (`personId`) REFERENCES `Person`(`personId`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `PersonContactDetail` ADD CONSTRAINT `PersonContactDetail_contactDetailsId_fkey` FOREIGN KEY (`contactDetailsId`) REFERENCES `ContactDetail`(`contactDetailsId`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `User` ADD CONSTRAINT `User_personId_fkey` FOREIGN KEY (`personId`) REFERENCES `Person`(`personId`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `Token` ADD CONSTRAINT `Token_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`userId`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `UsersNotes` ADD CONSTRAINT `UsersNotes_noteId_fkey` FOREIGN KEY (`noteId`) REFERENCES `Note`(`noteId`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `UsersNotes` ADD CONSTRAINT `UsersNotes_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `User`(`userId`) ON DELETE RESTRICT ON UPDATE CASCADE;
