import { Router } from 'express';

const router: Router = Router();

import { notesController } from '../controllers';
import {
  authMiddleware,
  tryCatchMiddleware,
  validateMiddleware,
} from '../middlewares';

import { notesValidator } from '../validators';

router.get('/', authMiddleware, tryCatchMiddleware(notesController.listNotes));
router.post(
  '/',
  authMiddleware,
  notesValidator.createNote,
  validateMiddleware,
  tryCatchMiddleware(notesController.createNote)
);

router.get(
  '/:noteId',
  authMiddleware,
  notesValidator.viewNote,
  validateMiddleware,
  tryCatchMiddleware(notesController.viewNote)
);

router.patch(
  '/:noteId',
  authMiddleware,
  notesValidator.updateNote,
  validateMiddleware,
  tryCatchMiddleware(notesController.updateNote)
);

router.delete(
  '/:noteId',
  authMiddleware,
  notesValidator.deleteNote,
  validateMiddleware,
  tryCatchMiddleware(notesController.deleteNote)
);

export default router;
