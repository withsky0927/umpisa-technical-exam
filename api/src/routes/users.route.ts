import { Router } from 'express';

const router: Router = Router();

import { authController, usersController } from '../controllers';
import {
  authMiddleware,
  tryCatchMiddleware,
  validateMiddleware,
} from '../middlewares';
import { authValidator, usersValidator } from '../validators';

router.post(
  '/login',
  usersValidator.login,
  validateMiddleware,
  tryCatchMiddleware(usersController.login)
);

router.post(
  '/signup',
  usersValidator.signup,
  validateMiddleware,
  tryCatchMiddleware(usersController.signup)
);

router.delete(
  '/logout',
  authMiddleware,
  tryCatchMiddleware(usersController.logout)
);

export default router;
