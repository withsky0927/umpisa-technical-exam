import { Router, Request, Response } from 'express';

import authRoute from './auth.route';
import usersRoute from './users.route';
import notesRoute from './notes.route';

const router: Router = Router();

router.use('/auth', authRoute);
router.use('/users', usersRoute);
router.use('/notes', notesRoute);

router.get('/', (req: Request, res: Response): Response => {
  const d: Date = new Date();
  return res.status(200).send(`Healthy since ${d.toString()}`);
});

export default router;
