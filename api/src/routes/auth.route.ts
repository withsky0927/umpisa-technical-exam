import { Router } from 'express';

const router: Router = Router();

import { authController } from '../controllers';
import {
  authMiddleware,
  tryCatchMiddleware,
  validateMiddleware,
} from '../middlewares';
import { authValidator } from '../validators';

router.patch(
  '/refresh-tokens',
  tryCatchMiddleware(authController.generateRefreshToken)
);

export default router;
