import { Request, Response, NextFunction } from 'express';
import _ from 'lodash';

import { jwt } from '../libs';
import { db } from '../config';
import { TokenType } from '../enums';

export default async (
  request: Request & { user?: { userId?: string } },
  response: Response,
  next: NextFunction
): Promise<Response | void> => {
  try {
    const authHeader: string = request.headers.authorization ?? '';

    if (_.isEmpty(authHeader)) {
      return response.status(401).json({
        status: false,
        message: 'Unauthorized',
      });
    }

    const accessToken = authHeader.replace('Bearer ', '');

    const accessTokenSecret = process.env.JWT_ACCESS_TOKEN_SECRET as string;

    const decoded = (await jwt.verify(accessToken, accessTokenSecret)) as {
      error: boolean;
      value: any;
    };

    const isTokenExpired: boolean =
      decoded.error && decoded.value.message === 'jwt expired';

    const itTokenMalformed: boolean =
      decoded.error && decoded.value.message !== 'jwt expired';

    if (isTokenExpired) {
      return response
        .status(400)
        .json({ status: false, message: 'Token expired.' });
    }

    if (itTokenMalformed) {
      return response
        .status(400)
        .json({ status: false, message: 'Invalid token' });
    }

    const { _id: userId } = decoded.value;

    const isRefreshTokenInDatabase = await db.token.findFirst({
      include: { User: true },
      where: { userId, type: TokenType.refresh },
    });

    if (_.isEmpty(isRefreshTokenInDatabase)) {
      return response
        .status(401)
        .json({ status: false, message: 'Unauthorized' });
    }

    request.user = {
      userId: userId,
    };

    next();
  } catch (error) {
    console.log(error);
    next(error);
  }
};
