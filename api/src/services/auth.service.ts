import { jwt } from '../libs';
import { db } from '../config';
import { TokenType } from '../enums';

const generateRefreshToken = async (data: { refreshToken: string }) => {
  try {
    const refreshTokenSecret = process.env.JWT_REFRESH_TOKEN_SECRET as string;
    const accessTokenSecret = process.env.JWT_ACCESS_TOKEN_SECRET as string;

    const decoded = (await jwt.verify(
      data.refreshToken,
      refreshTokenSecret
    )) as { error: boolean; value: any };

    const isTokenExpired: boolean =
      decoded.error && decoded.value.message === 'jwt expired';

    const isTokenMalformed: boolean =
      decoded.error && decoded.value.message !== 'jwt expired';

    if (isTokenExpired || isTokenMalformed) {
      return {
        error: true,
      };
    }

    const { _id: userId } = decoded.value;

    const jwtPayload = {
      _id: userId,
    };

    const newAccessToken = (await jwt.generate(jwtPayload, accessTokenSecret, {
      expiresIn: '15m',
    })) as string;

    const newRefreshToken = (await jwt.generate(
      jwtPayload,
      refreshTokenSecret,
      {
        expiresIn: '30d',
      }
    )) as string;

    const newToken = {
      value: newRefreshToken,
      updatedBy: userId,
    };

    await db.$transaction(async trx => {
      await trx.token.updateMany({
        data: newToken,
        where: { userId: userId, type: TokenType.refresh },
      });
    });

    return {
      accessToken: newAccessToken,
      refreshToken: newRefreshToken,
    };
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

export default { generateRefreshToken };
