import { ulid } from 'ulid';
import { db } from '../config';

const createNote = async (data: {
  note: { title: string; content: string; isImportant: number; date: Date };
  user: { userId: string };
}) => {
  try {
    const { note, user } = data;

    const userId: string = user.userId;
    const noteId: string = ulid();

    const [newNote] = await db.$transaction(async trx => {
      const newNote = await trx.note.create({
        select: {
          noteId: true,
          title: true,
          content: true,
          isImportant: true,
          date: true,
          createdBy: true,
          createdAt: true,

          updatedAt: true,
          updatedBy: true,
        },
        data: {
          ...note,
          noteId,
          createdBy: userId,
        },
      });

      await trx.usersNotes.create({ data: { userId, noteId } });

      return [newNote];
    });

    return newNote;
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

const listNotes = async (data: { user: { userId: string } }) => {
  try {
    const { user } = data;

    const result = await db.usersNotes.findMany({
      select: {
        note: {
          select: {
            noteId: true,
            title: true,
            content: true,
            isImportant: true,
            date: true,
            createdBy: true,
            createdAt: true,
            updatedAt: true,
            updatedBy: true,
          },
        },
      },
      where: {
        user: { userId: user.userId },
      },
    });

    const notes = result.map(value => ({ ...value.note }));

    return notes;
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

const viewNote = async (data: {
  note: { noteId: string };
  user: { userId: string };
}) => {
  try {
    const { note, user } = data;

    const result = await db.usersNotes.findFirst({
      select: {
        note: {
          select: {
            noteId: true,
            title: true,
            content: true,
            isImportant: true,
            date: true,
            createdBy: true,
            createdAt: true,
            updatedAt: true,
            updatedBy: true,
          },
        },
      },
      where: {
        note: { noteId: note.noteId },
        user: { userId: user.userId },
      },
    });

    return result?.note;
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

const updateNote = async (data: {
  note: {
    noteId?: string;
    title?: string;
    content?: string;
    isImportant?: number;
    date?: Date;
  };
  user: { userId: string };
}) => {
  try {
    const { note, user } = data;

    const noteId = note.noteId;

    delete note.noteId;

    const [updatedNote] = await db.$transaction(async trx => {
      const updatedNote: any = await trx.note.update({
        select: {
          noteId: true,
          title: true,
          content: true,
          isImportant: true,
          date: true,
          createdBy: true,
          createdAt: true,
          updatedAt: true,
          userNotes: {
            where: {
              noteId: noteId,
              userId: user.userId,
            },
          },
        },
        data: {
          ...note,
        },
        where: {
          noteId: noteId,
        },
      });

      return [updatedNote];
    });

    delete updatedNote?.userNotes;

    return updatedNote;
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

const deleteNote = async (data: {
  note: { noteId: string };
  user: { userId: string };
}) => {
  try {
    const { note, user } = data;

    console.log(note);

    await db.$transaction(async trx => {
      await trx.usersNotes.deleteMany({
        where: { noteId: note.noteId, userId: user.userId },
      });

      await trx.note.delete({
        where: {
          noteId: note.noteId,
        },
      });
    });

    return Promise.resolve(true);
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

export default {
  createNote,
  listNotes,
  viewNote,
  updateNote,
  deleteNote,
};
