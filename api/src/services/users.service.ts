import { ulid } from 'ulid';
import argon2 from 'argon2';
import _ from 'lodash';

import { db } from '../config';
import { jwt } from '../libs';
import { TokenType } from '../enums';

const signup = async (data: {
  username: string;
  email: string;
  password: string;
  confirmPassword: string;
  firstName: string;
  middleName: string | undefined;
  lastName: string;
}) => {
  try {
    const personId: string = ulid();
    const userId: string = ulid();
    const personContactDetailsId = ulid();

    const hashedPassword: string = await argon2.hash(data.confirmPassword);

    const personData = {
      personId,
      firstName: data.firstName,
      middleName: !data.middleName ? '' : data.middleName,
      lastName: data.lastName,
      createdBy: userId,
    };

    const userData = {
      personId,
      userId,
      username: data.username,
      password: hashedPassword,
      createdBy: userId,
    };

    const personContactDetailsData = {
      personId,
      personContactDetailsId,
      value: data.email,
      contactDetailsId: 1,
      createdBy: userId,
    };

    await db.$transaction(async trx => {
      const newPerson = await trx.person.create({ data: personData });

      const newPersonContactDetails = await trx.personContactDetail.create({
        data: personContactDetailsData,
      });

      const newUser = await trx.user.create({ data: userData });
    });

    return Promise.resolve(true);
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

const login = async (data: { username: string }) => {
  try {
    const { username } = data;

    const result = await db.user.findUnique({
      select: { userId: true },
      where: {
        username,
      },
    });

    const userId = result?.userId as string;

    const jwtPayload = {
      _id: result?.userId,
    };

    const refreshTokenSecret = process.env.JWT_REFRESH_TOKEN_SECRET as string;
    const accessTokenSecret = process.env.JWT_ACCESS_TOKEN_SECRET as string;

    const accessToken = (await jwt.generate(jwtPayload, accessTokenSecret, {
      expiresIn: '15m',
    })) as string;

    const refreshToken = (await jwt.generate(jwtPayload, refreshTokenSecret, {
      expiresIn: '30d',
    })) as string;

    const token = await db.token.findFirst({
      select: { value: true, tokenId: true },
      where: { type: TokenType.refresh, userId },
    });

    await db.$transaction(async trx => {
      if (token) {
        await trx.token.update({
          where: {
            userId: userId,
            tokenId: token.tokenId,
            type: TokenType.refresh,
          },

          data: {
            value: refreshToken,
            updatedBy: userId,
          },
        });
      } else {
        await trx.token.create({
          data: {
            tokenId: ulid(),
            userId: userId,
            type: TokenType.refresh,
            value: refreshToken,
            createdBy: userId,
          },
        });
      }
    });

    return {
      accessToken,
      refreshToken,
    };
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

const logout = async (data: { user: { userId: string } }) => {
  try {
    const { user } = data;

    await db.$transaction(async trx => {
      await trx.token.deleteMany({
        where: { userId: user.userId, type: TokenType.refresh },
      });
    });

    return Promise.resolve(true);
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

export default {
  login,
  signup,
  logout,
};
