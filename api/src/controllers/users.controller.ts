import { Request, Response } from 'express';

import { usersService } from '../services';

const login = async (
  request: Request,
  response: Response
): Promise<Response> => {
  const { body } = request;

  const result = await usersService.login(body);

  response.cookie('token', result.refreshToken, {
    httpOnly: true,
    sameSite: 'lax',
    maxAge: 2592000 * 1000,
  });

  return response.status(200).json({
    status: true,
    message: 'Login successfully.',
    data: result,
  });
};

const signup = async (
  request: Request,
  response: Response
): Promise<Response> => {
  const { body } = request;

  const result = await usersService.signup(body);

  return response
    .status(201)
    .json({ status: true, message: 'Sign up successfully.' });
};

const logout = async (
  request: Request & { user: { userId: string } },
  response: Response
): Promise<Response> => {
  const { user } = request;

  const result = await usersService.logout({ user });

  response.cookie('token', '', { maxAge: 0 });

  return response
    .status(200)
    .json({ status: true, message: 'Logout successfully.' });
};

export default {
  login,
  signup,
  logout,
};
