import { Request, Response } from 'express';
import _ from 'lodash';

import { authService } from '../services';

const generateRefreshToken = async (
  request: Request,
  response: Response
): Promise<any> => {
  const { token } = request.cookies;

  if (_.isEmpty(token)) {
    return response
      .status(403)
      .json({ status: false, message: 'Unauthorized.' });
  }

  const data = {
    refreshToken: token,
  };

  const result = await authService.generateRefreshToken(data);

  if (result.error) {
    return response.status(401).json({
      status: false,
      message: 'Invalid or Expired refresh token.',
    });
  }

  response.cookie('token', result.refreshToken, {
    httpOnly: true,
    sameSite: 'lax',
    maxAge: 2592000 * 1000,
  });

  return response.status(200).json({
    status: true,
    message: 'Token generated successfully.',
    data: result,
  });
};

export default { generateRefreshToken };
