import { Request, Response } from 'express';

import { notesService } from '../services';

const createNote = async (
  request: Request & { user: { userId: string } },
  response: Response
) => {
  const { body, user } = request;

  const note = body;

  const result = await notesService.createNote({ note, user });

  return response.status(201).json({
    status: true,
    message: 'Note created successfully.',
    data: result,
  });
};

const listNotes = async (
  request: Request & { user: { userId: string } },
  response: Response
) => {
  const { search } = request.query;
  const { user } = request;

  const result = await notesService.listNotes({ user });

  return response.status(200).json({
    status: true,
    message: 'Success.',
    data: result,
  });
};

const viewNote = async (
  request: Request & { user: { userId: string } },
  response: Response
) => {
  const { noteId } = request.query;
  const { user } = request;

  const result = await notesService.viewNote({
    note: { noteId: noteId as string },
    user,
  });

  return response.status(200).json({
    status: true,
    message: 'Success.',

    data: result,
  });
};

const updateNote = async (
  request: Request & { user: { userId: string } },
  response: Response
) => {
  const { body, user } = request;
  const { noteId } = request.params;

  const result = await notesService.updateNote({
    note: { ...body, noteId },
    user,
  });

  return response.status(200).json({
    status: true,
    message: 'Note updated successfully.',
    data: result,
  });
};

const deleteNote = async (
  request: Request & { user: { userId: string } },
  response: Response
) => {
  const { noteId } = request.params;
  const { user } = request;

  await notesService.deleteNote({
    note: { noteId: noteId as string },
    user,
  });
  return response.status(200).json({
    status: true,
    message: 'Success.',
  });
};

export default {
  createNote,
  listNotes,
  viewNote,
  updateNote,
  deleteNote,
};
