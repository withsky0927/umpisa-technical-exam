import nodemailer from 'nodemailer';

const transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: process.env.SENDER_EMAIL,
    pass: process.env.SENDER_EMAIL_PASSWORD,
  },
});

const sendMail = async (
  email: string,
  subj: string,
  from: string,
  html: string
) => {
  const mailOptions = {
    from: from ? `${from} <${from}>` : `Umpisa Inc. <${from}>`,
    to: email,
    subject: subj,
    html,
  };

  return new Promise((resolve, reject) => {
    transporter
      .sendMail(mailOptions)
      .then(r => {
        resolve(r);
      })
      .catch(e => {
        reject(e);
      });
  });
};

export default { sendMail };
