import jwt, { JwtPayload, Secret, SignOptions } from 'jsonwebtoken';

const generate = async (
  payload: JwtPayload,
  secret: Secret,
  options: SignOptions & { algorithm?: 'none' }
) => {
  try {
    return await new Promise((resolve, reject) => {
      jwt.sign(
        payload,
        secret,
        options,
        (err: Error | null, token: string | undefined) => {
          if (err) reject(err);
          resolve(token);
        }
      );
    });
  } catch (error) {
    console.log(error);
    throw new Error(String(error));
  }
};

const verify = async (payload: string, secret: Secret) =>
  new Promise((resolve, reject) => {
    jwt.verify(payload, secret, (err: Error | null, token: any) => {
      if (err) resolve({ error: true, value: err });
      resolve({ error: false, value: token });
    });
  });

export default { verify, generate };
