import authValidator from './auth.validator';
import usersValidator from './users.validator';
import notesValidator from './notes.validator';

export { authValidator, usersValidator, notesValidator };
