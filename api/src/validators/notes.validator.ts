import { body, query, param } from 'express-validator';
import { db } from '../config';
import _ from 'lodash';

const createNote = [
  body('title')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Title must be required.')
    .bail()
    .isString()
    .withMessage('Title must be a string.')
    .bail()
    .isLength({ max: 50 })
    .withMessage('Title must be must not exceed 50 characters.')
    .bail()
    .custom(async (value, { req }) => {
      const { user } = req;
      const title = value;

      const result = await db.note.findFirst({
        include: {
          userNotes: {
            where: {
              userId: user.userId,
              user: { userId: user.userId },
            },
          },
        },
        where: {
          title,
        },
      });

      if (!_.isEmpty(result)) {
        throw new Error('Title already used.');
      }
    })
    .trim()
    .escape(),

  body('content')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Content must be required.')
    .bail()
    .isString()
    .withMessage('Content must be a string')
    .bail()
    .trim()
    .escape(),

  body('isImportant')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Is Important must be required.')
    .bail()
    .isIn([0, 1])
    .withMessage('Is Important must be either 0 or 1')
    .bail()
    .toInt(),

  body('date')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Date must be required.')
    .bail()
    .isISO8601()
    .withMessage('Date must be in "yyyy-mm-dd" format')
    .bail()
    .toDate(),
];

const viewNote = [
  param('noteId')
    .notEmpty()
    .withMessage('Note id must be required.')
    .bail()
    .custom(async (value, { req }) => {
      const ulidChecker = /^[0-9A-Z]{26}$/;

      if (!ulidChecker.test(value)) {
        throw new Error('Note id must be valid id format');
      }
    })
    .bail()
    .custom(async (value, { req }) => {
      const { user } = req;
      const noteId = value;

      const result = await db.note.findUnique({
        include: {
          userNotes: {
            where: {
              noteId,
              userId: user.userId,
              user: { userId: user.userId },
            },
          },
        },
        where: {
          noteId,
        },
      });

      if (_.isEmpty(result)) {
        throw new Error('Note must be exist.');
      }
    }),
];

const updateNote = [
  param('noteId')
    .notEmpty()
    .withMessage('Note id must be required.')
    .bail()
    .custom(async (value, { req }) => {
      const ulidChecker = /^[0-9A-Z]{26}$/;

      if (!ulidChecker.test(value)) {
        throw new Error('Note id must be valid id format');
      }
    })
    .bail()
    .custom(async (value, { req }) => {
      const { user } = req;
      const noteId = value;

      const result = await db.note.findUnique({
        include: {
          userNotes: {
            where: {
              noteId,
              userId: user.userId,
              user: { userId: user.userId },
            },
          },
        },
        where: {
          noteId,
        },
      });

      if (_.isEmpty(result)) {
        throw new Error('Note must be exist.');
      }
    })
    .bail(),
  body('title')
    .optional()
    .isString()
    .withMessage('Title must be a string.')
    .bail()
    .isLength({ max: 50 })
    .withMessage('Title must be must not exceed 50 characters.')
    .bail()
    .custom(async (value, { req }) => {
      const { user } = req;
      const title = value;

      const result = await db.note.findFirst({
        include: {
          userNotes: {
            where: {
              userId: user.userId,
              user: { userId: user.userId },
            },
          },
        },
        where: {
          title,
        },
      });

      if (!_.isEmpty(result)) {
        throw new Error('Title already used.');
      }
    })
    .bail(),

  body('content')
    .optional()
    .isString()
    .withMessage('Content must be a string')
    .bail()
    .trim()
    .escape(),

  body('isImportant')
    .optional()
    .isIn([0, 1])
    .withMessage('Is Important must be either 0 or 1')
    .bail()
    .toInt(),

  body('date')
    .optional()
    .isISO8601()
    .withMessage('Date must be in "yyyy-mm-dd" format')
    .bail()
    .toDate(),
];

const deleteNote = [
  param('noteId')
    .notEmpty()
    .withMessage('Note id must be required.')
    .bail()
    .custom(async (value, { req }) => {
      const ulidChecker = /^[0-9A-Z]{26}$/;

      if (!ulidChecker.test(value)) {
        throw new Error('Note id must be valid id format');
      }
    })
    .bail()
    .custom(async (value, { req }) => {
      const { user } = req;
      const noteId = value;

      const result = await db.note.findUnique({
        include: {
          userNotes: {
            where: {
              noteId,
              userId: user.userId,
              user: { userId: user.userId },
            },
          },
        },
        where: {
          noteId,
        },
      });

      if (_.isEmpty(result)) {
        throw new Error('Note must be exist.');
      }
    }),
];

export default {
  createNote,
  viewNote,
  updateNote,
  deleteNote,
};
