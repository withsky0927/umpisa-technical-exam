import { body, param, query } from 'express-validator';
import _ from 'lodash';
import argon2 from 'argon2';

import { db } from '../config';

const signup = [
  body('username')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Username must be required.')
    .bail()
    .custom(async (value, { req }) => {
      const result = await db.user.findUnique({
        select: { userId: true },
        where: { username: value },
      });

      if (!_.isEmpty(result?.userId)) {
        throw new Error('Username are already used.');
      }
    })
    .trim(),
  body('email')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Email must be required')
    .bail()
    .isEmail()
    .withMessage('Email must be valid email address format.')
    .bail()
    .custom(async (email, { req }) => {
      const result = await db.personContactDetail.findFirst({
        select: { value: true },
        where: {
          value: email,
          OR: [
            {
              value: email.toLowerCase(),
            },
          ],
          contactDetail: {
            contactDetailsId: 1,
          },
        },
      });

      if (!_.isEmpty(result?.value)) {
        throw new Error('Email are already used.');
      }
    })
    .trim(),
  body('password')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Password must be required.')
    .bail()
    .matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&^])[A-Za-z\d@$!%*#?&^]{8,}$/)
    .withMessage(
      'Password must be minimum eight characters, at least one letter, one number and one allowed special character: @$!%*#?&^'
    )
    .bail(),
  body('confirmPassword')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Confirm password must be required.')
    .bail()
    .custom(async (value, { req }) => {
      const isNotMatch: boolean = value !== req.body.password;

      if (isNotMatch) {
        throw new Error('Confirm password must be match with password field.');
      }
    }),
  body('firstName')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('First Name must be required')
    .bail()
    .matches(/^[A-Za-z ]*$/)
    .withMessage(
      'First name must be alphabet characters with or without space only.'
    )
    .bail()
    .isLength({ max: 50 })
    .withMessage('First name must be must not exceed 50 characters.')
    .bail()
    .trim()
    .escape(),
  body('middleName')
    .optional()
    .matches(/^[A-Za-z ]*$/)
    .withMessage(
      'Middle name must be alphabet characters with or without space only.'
    )
    .bail()
    .isLength({ max: 50 })
    .withMessage('Middle name must be must not exceed 50 characters.')
    .bail()
    .trim()
    .escape(),
  body('lastName')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('First Name must be required.')
    .bail()
    .matches(/^[A-Za-z ]*$/)
    .withMessage(
      'Last name must be alphabet characters with or without space only.'
    )
    .bail()
    .isLength({ max: 50 })
    .withMessage('Last name must be must not exceed 50 characters.')
    .bail()
    .trim()
    .escape(),
];

const login = [
  body('username')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Username must be required.')
    .bail()
    .custom(async (value, { req }) => {
      const result = await db.user.findUnique({
        select: { username: true },

        where: {
          username: value,
        },
      });

      if (_.isEmpty(result)) {
        throw new Error('Username must be valid.');
      }
    }),
  body('password')
    .notEmpty({ ignore_whitespace: false })
    .withMessage('Password must be required.')
    .bail()
    .custom(async (value, { req }) => {
      const { username } = req.body;
      const password = value;

      const result = await db.user.findUnique({
        select: { password: true, username: true },

        where: {
          username: username,
        },
      });

      if (_.isEmpty(result)) {
        throw new Error('Password must be valid');
      }

      const isValidPassword = await argon2.verify(result.password, password);

      if (!isValidPassword) {
        throw new Error('Password must be valid');
      }
    })
    .bail(),
];

export default {
  signup,
  login,
};
